Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: color-tools
Upstream-Contact: Austin Ziegler <halostatue@gmail.com>, Matt Lyon <matt@postsomnia.com>
Source: http://rubyforge.org/projects/color/

Files: *
Copyright: Copyright © 2005-2014 Austin Ziegler, Matt Lyon and other contributors
License: MIT-ColorTools

Files: debian/*
Copyright: Copyright © 2007-2011, Gunnar Wolf <gwolf@debian.org>
License: MIT

License: MIT-ColorTools
 Color::Palette was developed based on techniques described by Andy
 "Malarkey" Clarke[1], implemented in JavaScript by Steve G. Chipman at
 SlayerOffice[2] and by Patrick Fitzgerald of BarelyFitz[3] in PHP.
 .
 == Licence
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Soft-
 ware"), to deal in the Software without restriction, including without
 limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the following
 conditions:
 .
 * The names of its contributors may not be used to endorse or promote
   products derived from this software without specific prior written
   permission.
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 .
 [1] http://www.stuffandnonsense.co.uk/archives/creating_colour_palettes.html
 [2] http://slayeroffice.com/tools/color_palette/
 [3] http://www.barelyfitz.com/projects/csscolor/

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies of the Software, its documentation and marketing & publicity
 materials, and acknowledgment shall be given in the documentation, materials
 and software packages that this Software was used.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
